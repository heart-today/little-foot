extends "res://addons/gut/test.gd"

func before_each():
	gut.p("ran setup", 2)

func after_each():
	gut.p("ran teardown", 2)

func before_all():
	gut.p("ran run setup", 2)

func after_all():
	gut.p("ran run teardown", 2)

func test_change_state():
	var current = { a = 4, b = { c = 9, d = 10} }
	var delta = { a = 3, b = { c = 2 } }
	var changed = State.change_state(current, delta)
	assert_not_null(changed)
	assert_eq(changed.a, delta.a)
	assert_eq(changed.b.c, delta.b.c)
	assert_eq(changed.b.d, current.b.d)

func test_next_state():
	var current = { 
		jump = 'jump',
		duck = {
			cat = 27,
			dog = 19
		}
			
	}
	var states = {
		jump = {
			height = 5,
			jump = 'really_jump',
			duck = {
				dog = -3
			},
			give_to = {
				'*': 'magic',
				duck = 'happy'
			}
		},
		really_jump = {
			height = 10,
			jump = 'tired',
			duck = {
				cat = -11
			}
		},
		happy = {
			smile = 17
		},
		magic = {
			wand = "silver"
		}
	}
	current = State.next_state(states, current, 'jump')
	assert_eq(current.height, 5)
	assert_eq(current.jump, 'really_jump')

	assert_eq(current.duck.cat, 27)
	assert_eq(current.duck.dog, -3)
	
	current = State.next_state(states, current, 'jump')
	assert_eq(current.height, 10)
	assert_eq(current.jump, 'tired')

	assert_eq(current.duck.cat, -11)
	assert_eq(current.duck.dog, -3)
	
	current = State.next_state(states, current, 'give_to', 'duck')
	assert_eq(current.smile, 17)

	# Test wildcard matching
	current = State.next_state(states, current, 'give_to', 'banana')
	assert_eq(current.wand, "silver")
