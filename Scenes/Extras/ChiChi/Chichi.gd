extends Node2D

signal talk(dialog, byCharacter, forCharacter)
signal dropoff(character)

var velocity = Vector2.ZERO

func picked_up_by(character : Node):
	pass
	
func pickup(character : Node):
	add_child(character)
	character.picked_up_by(self)


func can_be_picked_up_by(character : Node):
	return true
	
func move(dir : Vector2):
	pass

func set_idle_state(state : String):
	pass

func find_dialog_by_character_name(name : String):
	pass

func want_character(character : Node):
	# Greedy!!!
	return true
	
func talk(dialog, byCharacter : Node, forCharacter : Node):
	emit_signal("talk", byCharacter, forCharacter)
	
func meet(character : Node):
	var dialog = find_dialog_by_character_name(character.name)
	if dialog != null:
		talk(dialog, self, character)

	if want_character(character):
		pickup(character)

