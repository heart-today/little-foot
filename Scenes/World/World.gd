extends Node2D

export var conversation_pause = 3
export var GIVE_FACTOR = 5
export var COLLISION_OFF_TIME = 0.5

onready var conversationPlayer = $CanvasLayer/ConversationPlayer
onready var littleFoot = $LittleFoot
onready var collision = $LittleFoot/CollisionShape2D
onready var takePosition = $LittleFoot/TakenPosition
onready var garden = $Garden


func make_conversation(texts : Array):
	var conversation = []
	for text in texts:
		conversation.append({
			speaker = littleFoot.name,
			text = text,
			pause = conversation_pause
		})
	return conversation
	
func _ready():
	conversationPlayer.player = littleFoot

func receive(character : Node):
	# TODO: Check to make sure isn't giving off into space...
	add_child(character)
	character.position = to_local(takePosition.global_position)
	character.velocity = littleFoot.velocity * GIVE_FACTOR
	character.get_node("CollisionShape2D").disabled = false
	collision.disabled = true
	yield(get_tree().create_timer(COLLISION_OFF_TIME), "timeout")
	collision.disabled = false


func _on_LittleFoot_give_to(whatCharacter, toCharacter):
	if not toCharacter:
		receive(whatCharacter)


func _on_RockyOutcropping_converse(converse):
	conversationPlayer.play(converse)


func _on_Garden_converse(converse):
	conversationPlayer.play(converse)


func _on_FairyForest_converse(converse):
	conversationPlayer.play(converse)


func _on_Dam_converse(converse):
	conversationPlayer.play(converse)


func _on_Bedroom_converse(converse):
	conversationPlayer.play(converse)
