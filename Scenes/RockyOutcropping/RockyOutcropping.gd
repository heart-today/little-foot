extends Node2D

signal converse(conversation)


func _on_Oregano_converse(conversation):
	emit_signal("converse", conversation)

func _on_Rock_converse(conversation):
	emit_signal("converse", conversation)
