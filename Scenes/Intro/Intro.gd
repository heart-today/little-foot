extends Node2D

export var duck_time = 5

onready var conversationPlayer = $ConversationPlayer
onready var player = $LittleFoot
onready var givingPosition = $LittleFoot/GivingPosition
onready var playerSprite = $LittleFoot/AnimatedSprite
onready var ducky := $Ducky

func _ready():
	conversationPlayer.player = player
	ducky.player = player
	
	ducky.connect("converse", conversationPlayer, "_on_converse")
	player.connect("cancel", self, "next_scene")
	
	yield(conversationPlayer.play({ exchange = [{
		speaker = "Narrator",
		text = "Welcome to the Adventures of Little Foot",
		pause = duck_time
		}]}), "completed")

	ducky.speak("Hi, I'm [color=red]Ducky![/color]")
	
	ducky.visible = true	

	player.meet(ducky)

func receive(character : Node):
	# TODO: Check to make sure isn't giving off into space...
	add_child(character)
	character.position = to_local(givingPosition.global_position)

# Pretending to be Terrain
func _on_TestPlayer_give_to(whatCharacter, toCharacter):
	if not toCharacter:
		receive(whatCharacter)
		
		player.cheer()
		
		yield(conversationPlayer.play({exchange = [{
			speaker = "Little Foot",
			text = "YAY!!!!!!!",
			pause = duck_time
			},
			{
			speaker = "Narrator",
			text = "It looks like you are ready to see the world!",
			pause = duck_time
			}]}), "completed")

	player.cheer(false)

	next_scene()
	
func next_scene():	
	get_tree().change_scene("res://Scenes/NewWorld/NewWorld.tscn")

