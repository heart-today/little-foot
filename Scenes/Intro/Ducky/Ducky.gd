extends Character2D

var player = null

func _on_TestPlayer_meet(character):
	if character == self:
		next_state(MEET)

func _on_TestPlayer_ask(character):
	# If always getting this signal, and checking if for you,
	# you can un/register within meet above...
	if character == self:
		next_state(ASK)
