extends Node2D

signal converse(conversation)

func _on_DreamingGirl_converse(conversation):
	emit_signal("converse", conversation)


func _on_ProstheticLittleFoot_converse(conversation):
	emit_signal("converse", conversation)
