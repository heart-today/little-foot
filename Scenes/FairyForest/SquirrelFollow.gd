extends PathFollow2D

onready var tween = $Tween

# Called when the node enters the scene tree for the first time.
func _ready():
	tween.interpolate_property(self, "unit_offset", 0, 1, 6,
	 Tween.TRANS_LINEAR, Tween.EASE_IN_OUT)
	tween.repeat = true
	tween.start()


func _on_Squirrel_start_following():
	tween.resume(self, "unit_offset")


func _on_Squirrel_stop_following():
	tween.stop(self, "unit_offset")
