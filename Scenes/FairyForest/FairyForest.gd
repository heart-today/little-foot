extends Node2D

signal converse(conversation)


func _on_Squirrel_converse(conversation):
	emit_signal("converse", conversation)


func _on_BearDog_converse(conversation):
	emit_signal("converse", conversation)
