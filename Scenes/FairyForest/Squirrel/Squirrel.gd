extends Character2D

signal stop_following()
signal start_following()

# MEET will stop_swimming

func stop_following(state):
	emit_signal("stop_following")

func start_following(state):
	emit_signal("start_following")
