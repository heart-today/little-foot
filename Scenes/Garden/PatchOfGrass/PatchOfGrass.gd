extends Character2D

signal grow_grass(position, direction)

var grow_grass := false

var last_position := Vector2.ZERO

func _physics_process(_delta):
	
	if grow_grass:
		# TODO Is zero okay?
		emit_signal("grow_grass", global_position, (global_position - last_position).normalized())
		last_position = global_position

func release():
	.release()
	
	grow_grass = true
		
	return self
