extends Node2D

signal converse(conversation)
signal grow_grass(position, direction)

func _on_PatchOfGrass_converse(conversation):
	emit_signal("converse", conversation)

func _on_PatchOfGrass_grow_grass(position, direction):
	emit_signal("grow_grass", position, direction)

func _on_Tree_converse(conversation):
	emit_signal("converse", conversation)


func _on_Gardener_converse(conversation):
	emit_signal("converse", conversation)


func _on_PottedOregano_converse(conversation):
	emit_signal("converse", conversation)


func _on_Acorn_converse(conversation):
	emit_signal("converse", conversation)


func _on_YellowPot_converse(conversation):
	emit_signal("converse", conversation)


func _on_RedPot_converse(conversation):
	emit_signal("converse", conversation)


func _on_Pineapple_converse(conversation):
	emit_signal("converse", conversation)


func _on_Tomato_converse(conversation):
	emit_signal("converse", conversation)
