extends KinematicBody2D

signal meet(character)
signal give_to(whatCharacter, toCharacter)
signal take(character)
signal rejected_by(character)
signal ask(character)

signal cancel

enum {
	MOVE,
	CHEER
}

var meet : Character2D = null
var taken : Character2D = null

export var ACCELERATION = 500
export var MAX_SPEED = 80
export var FRICTION = 500

var velocity = Vector2.ZERO
var direction = Vector2.ZERO
var state = MOVE

onready var takenPosition = $TakenPosition
onready var animatedSprite = $AnimatedSprite
onready var audioStreamPlayer = $AudioStreamPlayer

var debug = false
	
func _physics_process(delta):
	var input_vector = Vector2.ZERO
	
	input_vector.x = Input.get_action_strength("ui_right") - Input.get_action_strength("ui_left")
	input_vector.y = Input.get_action_strength("ui_down") - Input.get_action_strength("ui_up")
	
	if Input.is_action_just_pressed("ui_cancel"):
		emit_signal("cancel")

	if Input.is_action_just_pressed("ask"):
		if meet:
			ask()
		
	if Input.is_action_just_pressed("take"):
		if meet:
			take()
		
		
	if Input.is_action_just_pressed("give"):
		if taken:
			give()
			
	if Input.is_action_just_pressed("ui_focus_next"):
		if debug:
			debug = false
			ACCELERATION = 500
			MAX_SPEED = 80
			FRICTION = 500
		else:
			debug = true
			ACCELERATION = 2000
			MAX_SPEED = 500
			FRICTION = 500
			
		var idx = AudioServer.get_bus_index("Music")
		
		AudioServer.set_bus_mute(idx, not AudioServer.is_bus_mute(idx))
		
	if state == MOVE:
		move(input_vector, delta)

func cheer(cheer := true):
	if cheer:
		state = CHEER
		animatedSprite.play("Hop")
	else:
		state = MOVE


func move(input_vector, delta):

	if input_vector != Vector2.ZERO:
		direction = input_vector
	
		animatedSprite.play("Hop")
		if not audioStreamPlayer.playing:
			audioStreamPlayer.play()
		
		velocity = velocity.move_toward(input_vector.normalized() * MAX_SPEED, ACCELERATION * delta)
	else:
		animatedSprite.stop()
		audioStreamPlayer.stop()
		animatedSprite.frame = 0
		
		velocity = velocity.move_toward(Vector2.ZERO, FRICTION * delta)
	
	animatedSprite.flip_h = direction.x < 0
	
	velocity = move_and_slide(velocity)
	
func meet(character : Character2D):
	if character == meet or (taken and character == taken):
		return
		
	if character:
		character.meet()
	else:
		meet.leave()
		
	meet = character
	
	emit_signal("meet", meet)

func take():
	assert(meet)

	if meet.can_take():
		if taken:
			give()
	
		_take_meet()
	
		emit_signal("take", taken)
		
		# Meet might not be null if meet returned another Character in release()
		if meet == null:
			emit_signal("meet", null)
	else:
		emit_signal("rejected_by", meet)

func _take_meet():
	assert(meet)
	
	# This generally allows meet to remove itself from scene tree
	# and also allows meet to give a child Character,
	# like for the Gardener	
	taken = meet.release()

	add_child(taken)
	
	taken.position = takenPosition.position

	if taken == meet:	
		meet = null

func release():
	# Reset character so can be taken by other characters or Terrain
	assert(taken)
	taken.release()
	
	# remove_child(taken)
	
	var released = taken	
	taken = null
	return released

func give():
	assert(taken)
	var released = release()
	assert(released)  # Shouldn't be able to give unless has taken Character
	assert(taken == null)
	var giving_to = meet
	if giving_to and not giving_to.receive(released):
		giving_to = null
		
	# If no giving_to, then Terrain will take released
	emit_signal("give_to", released, giving_to)
	
	emit_signal("take", null)

func ask():
	assert(meet)
	meet.ask()
	
	emit_signal("ask", meet)


func _on_MeetArea_body_entered(body):
	if body is Character2D:
		meet(body)


func _on_MeetArea_body_exited(body):
	if body is Character2D and body == meet:
		meet(null)
