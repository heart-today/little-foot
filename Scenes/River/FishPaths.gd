extends Node2D

export var FOLLOW_SCALE = 100

func _physics_process(delta):
	for path in get_children():
		for follow in path.get_children():
			var f : PathFollow2D = follow
			f.offset += FOLLOW_SCALE * delta
			

func _on_Fish3_start_swimming(fish):
	pass # Replace with function body.


func _on_Fish3_stop_swimming(fish):
	pass # Replace with function body.
