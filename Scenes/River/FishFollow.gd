extends PathFollow2D


# Declare member variables here. Examples:
# var a = 2
# var b = "text"

onready var tween = $Tween

# Called when the node enters the scene tree for the first time.
func _ready():
	tween.interpolate_property(self, "unit_offset", 0, 1, 6,
	 Tween.TRANS_LINEAR, Tween.EASE_IN_OUT)
	tween.repeat = true
	tween.start()


func _on_Fish3_start_swimming():
	tween.resume(self, "unit_offset")

func _on_Fish3_stop_swimming():
	tween.stop(self, "unit_offset")
