extends Node2D

signal converse(conversation)

func _on_Beaver_converse(conversation):
	emit_signal("converse", conversation)


func _on_Dam_converse(conversation):
	emit_signal("converse", conversation)


func _on_Fish3_converse(conversation):
	emit_signal("converse", conversation)


func _on_Raft_converse(conversation):
	emit_signal("converse", conversation)


func _on_Log_converse(conversation):
	emit_signal("converse", conversation)
