extends Character2D

signal stop_swimming()
signal start_swimming()

# MEET will stop_swimming

func stop_swimming(state):
	emit_signal("stop_swimming")

func start_swimming(state):
	emit_signal("start_swimming")


