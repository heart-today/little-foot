extends TileMap

enum {
	DIRT_WITH_FENCE,
	GRASS,
	ROCKY,
	RIVER_DAM
	# TODO And more to come...
}

func grow_grass(from : Vector2, towards : Vector2):
	grow_tile(from, towards, GRASS)
	
func grow_tile(from : Vector2, towards : Vector2, tile_index):
	# Let's create a cell!
	var mapPos = world_to_map(to_local(from))

	var index = get_cellv(mapPos)
	if index != TileMap.INVALID_CELL:
		mapPos += towards

		index = get_cellv(mapPos)
		if index == TileMap.INVALID_CELL:
			set_cellv(mapPos, tile_index)
			update_bitmask_area(mapPos)

#		for i in range(-1, 2):
#			for j in range(-1, 2):
#				var delta = mapPos + Vector2(i, j)
#
#				# Only grow over the Void
#				index = get_cellv(delta)
#				if index == TileMap.INVALID_CELL:
#					set_cellv(delta, tile_index)
#					update_bitmask_area(delta)
