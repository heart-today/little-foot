extends Node2D

export var GIVE_FACTOR = 5
export var COLLISION_OFF_TIME = 0.5

onready var conversationPlayer = $CanvasLayer/ConversationPlayer
onready var littleFoot = $LittleFoot
onready var collision = $LittleFoot/CollisionShape2D
onready var takePosition = $LittleFoot/TakenPosition
onready var garden = $Garden

onready var tileMap = $TileMap

func _ready():
	conversationPlayer.player = littleFoot

func receive(character : Node):
	# TODO: Check to make sure isn't giving off into space...
	add_child(character)
	character.position = to_local(takePosition.global_position)
	character.velocity = littleFoot.velocity * GIVE_FACTOR
	character.get_node("CollisionShape2D").disabled = false
	collision.disabled = true
	yield(get_tree().create_timer(COLLISION_OFF_TIME), "timeout")
	collision.disabled = false


func _on_LittleFoot_give_to(whatCharacter, toCharacter):
	if not toCharacter:
		receive(whatCharacter)

func _on_RockyOutcropping_converse(conversation):
	conversationPlayer.play(conversation)


func _on_Garden_converse(conversation):
	conversationPlayer.play(conversation)


func _on_Garden_grow_grass(position, direction):
	tileMap.grow_grass(position, direction)


func _on_FairyForest_converse(conversation):
	conversationPlayer.play(conversation)


func _on_River_converse(conversation):
	conversationPlayer.play(conversation)


func _on_Bedroom_converse(conversation):
	conversationPlayer.play(conversation)
