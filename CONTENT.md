# Content

Documenting the process of converting [Story](STORY.md) into a working [Demo](http://theirtemple.com/web/little-foot/little-foot.html)

## The Two Choices

Looking at this, the basic story can be told with only 2 Scenes/Terrains:
- RockyOutcropping
- Bedroom

The story becomes Little Foot looking for the LostBody, asking Rock, Oregano, and GrassPatch if they have seen the LostBody, or would they like to be the LostBody?

By uniting Oregano and GrassPatch, GrassPatch rewards LittleFoot with GrassSeeds, and LittleFoot can create GrassTiles and reach the Bedroom.

Waking up, the DreamingWoman embraces ProstheticLittleFoot.

The two choices then are:
  - Polish 
    - RockyOutcropping
    - Bedroom
  - More Minimal
    - Garden
    - Dam
    - River
    - FantasyForest
    - Void

The basic idea is to have the minimum for each character by 1pm PST Thursday 2021 March 18.

This gives us time to POLISH code and content until submission by 1pm PST Sunday 2021 March 21.

##  Most Vigorating Project or Minimal Viable Project

What is the minimal?

For animations, 2 frames per state minimum.

For moving, right 2 frames minimum (since can reverse for right, and just move up/down while facing left/right)

One background sound per character.

One state sound per character state.

## Terrain Characters

Copied from Source of Truth [Story Terrain Characters](STORY.md#terrain-characters)


- RockyOutcropping
  - Rock
  - Oregano
  - GrassPatch
  - GrassSeeds
  - GrassTile
- Void
  
  Nothing for Void as yet...
- Garden
  - Gardener (G)
  - PotForPlant (P4P)
  - PottedOregano (PO)
  - Nuts (N&S)
  - Tomato (T)
  - Pineapple (Pine)
- FantasyForest
  - Squirrel(s)
  - ChiChi (BearDog/DogBear)
  - Tree
- Dam
  - BigBarryTheBeaver
  - RaftOfTwigs
- River
- Bedroom
  - DreamingWoman
  - LittleProstheticFoot

## Document Template

- Terrain
  - Sounds
  - Images
  - Character
    - State
      - Sounds
      - Images

## Godot Project Scene Template

- Scenes
  - Terrain
    - Terrain.tscn
    - Terrain.gd
    - background.wav
    - dialog.json
    - dialog_font.ttf
    - Character
      - Character.tscn
      - Character.gd
      - state_named_sound.wav
      - state_other_sound.ogg
      - state_sprite_sheet.png
      - state_tileset.png
      - conversation.json
      - conversation_font.ttf
   - NonterrainScene (like ConversationPlayer)



## Prioritized Content As Task List

The item can NOT be checked until [demoable](http://theirtemple.com/web/little-foot/little-foot.html)

- [ ] RockyOutcropping
  - [ ] conversation
  - [ ] GreyRockyTileset
  - [ ] LittleFoot
    - [ ] conversation
    - [ ] Idle
      - [x] spritesheet
      - [ ] sound
    - [ ] Move Right
      - [x] spritesheet
      - [ ] sound
    - [ ] Move Left (reverse Move Right)
      - [ ] spritesheet
      - [ ] sound
    - POLISH
      - [ ] Move Up
      - [ ] Move Down
  - [ ] Rock
    - [ ] conversation
    - [ ] Idle
      - [ ] spritesheet
      - [ ] sound
  - [ ] Oregano
    - [ ] conversation
    - [ ] Idle
      - [ ] spritesheet
      - [ ] sound
  - [ ] GrassPatch
    - [ ] conversation
    - [ ] Idle
      - [ ] spritesheet
      - [ ] sound
  - [ ] GrassSeeds
    - [ ] conversation
    - [ ] Idle
      - [ ] spritesheet
      - [ ] sound
  - [ ] GrassTile
    - [ ] conversation
    - [ ] Placing Tile
      - [ ] GrassTileset
      - [ ] sound
- [ ] Bedroom
  - [ ] bedroom_background_image.png
  - [ ] DreamingWoman
    - [ ] conversation
    - [ ] Waking Up
      - [ ] amputated_woman_image.png
      - [ ] sound
    - [ ] PickingUpLittleProstheticFoot
  - [ ] LittleProstheticFoot
    - [ ] conversation
    - [ ] Idle
      - [ ] spritesheet
      - [ ] sound
- [ ] Garden
  - [ ] Gardener (G)
  - [ ] PotForPlant (P4P)
  - [ ] PottedOregano (PO)
  - [ ] Nuts (N&S)
  - [ ] Tomato (T)
  - [ ] Pineapple (Pine)
- [ ] Dam
  - [ ] BigBarryTheBeaver
  - [ ] RaftOfTwigs
- [ ] River
- [ ] FantasyForest
  - [ ] Squirrel(s)
  - [ ] ChiChi (BearDog/DogBear)
  - [ ] Tree
- [ ] Void
  - [ ] Bat(s)


