[[_TOC_]]

Based on [Story Summary](STORY.md#Story Summary]

# Scenes

  
## Message

### params
- speaker
- text
- pause (seconds float)
### signals
### methods

## Conversation

### params
### signals
### methods
- all_messages()

## ActionBar

End up handling visibility of buttons within ConversationPlayer

### params
### signals
### methods
  
## ConversationPlayer

### params
- player
### signals
### methods
- play(Conversation)
- show_character_icon(character, side = LEFT)
- on_Player_meet(Character)
  ```
  actionBar.askButton.visible = Character
  actionBar.takeButton.visible = Character
  
  character = Character
  if character:
    show_character_icon(character.get_conversation_icon())
  ```
- on_Player_take(Character)
  ```
  actionBar.giveButton.visible = Character  
  ```
- on_AskButton_pressed
  ```
  conversation = player.get_conversation()
  if conversation:
     play(conversation)
  ```
- on_TakeButton_pressed
  ```
  player.take()
  ```  
- on_GiveButton_pressed
  ```
  player.give()
  ```
  
## Character

### params
### signals
### methods
- get_conversation_icon() -> Sprite
- can_take() -> bool
- get_conversation() -> Conversation
- release()
  ```
  Get ready to be taken by Player or Terrain
  ```
- receive(Character) -> bool
  ```
  # Default
  return false
  
  # So many things to be done here...
  # grassPatch.receive(Oregano)
  ```

## Terrain (extends Character?)

Terrain handles Player talking to himself,
and Player giving Characters to Terrain (not to another Character)

### params
- player
### signals
### methods
- get_conversation() -> Conversation
- receive(Character)
  ```
  # Place the Character on the ground just outside of Player
  ```
- on_Player_give_to(Character what, Character to)
  ```
  if not to:
    receive(what)
  ```


## Player

### params
- meet : Character = null
- taken : Character = null
- terrain : Character = null
### signals
- meet(Character character)
- give_to(Character what, Character to)
- take(Character)

### methods
- get_conversation() -> Conversation
  ```
  conversation = null
  if meet:
    conversation = meet.get_conversation()
  else:
    conversation = terrain.get_conversation()
  return conversation
  ```
- release()
  ```
  # Reset character so can be taken by other characters or Terrain
  released = taken
  # Do whatever...?
  taken = null
  return released
  ```
- meet(character : Character = null)
  ```
  meet = character
  emit_signal("meet", meet)
  ```
- ask()
  ```
  emit_signal("ask", meet)
  ```
- take()
  ```
  assert(meet)

  if taken:
     give()

  if meet.can_take():
    meet.release()
    # Add meet to self
     
  emit_signal("take", meet)
  ```
- give()
  ```
  var released = release()
  assert(released)  # Shouldn't be able to give unless has taken Character
  var giving_to = meet
  if giving_to:
     if not giving_to.receive(released):
     	giving_to = null

  # If no meet, then Terrain will take released
  emit_signal("give_to", taken, giving_to)
  ```
  
# Story Summary As Code

1. LittleFoot starts in RockyOutcropping, traumatized by LostBody.
  ```
  player.meet()

  conversation = conversationPlayer.player.get_conversation()
  conversationPlayer.play(conversation)
  ```
2. Meets Rock, asks if they have seen the LostBody or would like to be the LostBody?
  ```
  player.on_MeetArea_entered(area)
    player.meet(get_character_from_area(area))

  # AskButton now visible
  # Player clicks AskButton
  conversationPlayer.on_AskButton_pressed
  
  # Dialog displayed

  # TakeButton now visible
  # Player clicks TakeButton
  conversationPlayer.on_TakeButton_pressed()
  player.take()

  # View Rock above LittleFoot
  # GiveButton now visible
  # Player clicks GiveButton
  on_GiveButton_pressed
  player.give()

  # Terrain accepts Rock
  ```
3. Meets Oregano, asks if they have seen the LostBody or would like to be the LostBody?
  ```
  # Same as Rock above
  # LittleFoot can try give Rock to Oregano
  # LittleFoot can try give Oregano to Rock
  # LittleFoot can take Oregano while carrying Rock
  ```
  
4. Gives Oregano to GrassPatch who gives LittleFoot GrassSeeds for creating GrassTiles to cross the Void.  
  ```
  # Same as Oregano above, and then...
  #
  player.give()
  grassPatch.receive(oregano)
    grassSeeds.release()
    player.meet(grassSeeds)
    player.take()

  player.move()
    if player.taken == grassSeeds:
      # Now can have GrassTileMap extend GreenTiles as LittleFoot moves
      create_grass_tile_as_needed()
  ```

5. LittleFoot meets the Gardener, who needs Oregano to give to BigBarryThe Beaver.

<TO BE CONTINUED>

# Questions

1. Can have Conversation with Terrains?

  Yes!  This is how LittleFoot talks to himself...

2. Can batch Player methods for Story Summary As Code #4?

   Yes.

   Assuming we can...LOL

3. Can have MEET conversation as well as ASK conversation?

  No.
  
  Right now, always triggered via AskButton so player.get_conversation() calls meet.get_conversation() or terrain.get_conversation()

  No way to differentiate...
  
- If have something, and try to take something which is untakable, does LittleFoot keep original thing or drop it on the ground?

  Taken item ends up on Terrain.
  
  LittleFoot gives to be able to take, so original thing will be on Terrain.