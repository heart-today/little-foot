# Little Foot

The Adventures of Little Foot, Looking for Completion in an Incomplete World

The project for submission to [Godot Wild Jam #31](https://godotwildjam.com)

All are welcome!  

Join us in [Falling Leaves on Discord](https://discord.gg/vwS5bgvQgk)

## Demo

[LittleFoot on Itch.io](https://hanumanjiyogi.itch.io/little-foot)!

Continuing development [Demo for Adventures of Little Foot](http://theirtemple.com/web/little-foot/little-foot.html) hosted on [Their Temple](http://theirtemple.com)

## Intentions

To gratefully give and receive help, find wonderful people to work together, give together, to breathe life into Little Foot and friends, continually expand the [Demo](http://theirtemple.com/web/little-foot/little-foot.html), implement the [Story](https://gitlab.com/heart-today/little-foot/-/blob/master/STORY.md), to bring the [Map](https://gitlab.com/heart-today/little-foot/-/blob/master/raw/map_from_aggie.png) into the real, merge [Architecture](https://gitlab.com/heart-today/little-foot/-/blob/master/ARCHITECTURE.md) and [Content](https://gitlab.com/heart-today/little-foot/-/blob/master/CONTENT.md) inside of [Godot](https://godotengine.org) with grace and style, and have fun and spread joy, expanding from our experience in the [Godot Wild Jam](https://godotwildjam.com)


![Start Map](/raw/start_map_from_aggie.png)
![End Map](/raw/end_map_from_godot.png)


## Features

## Installation

## Attributions

### Code

- SpeechBubble extended from [Godot Animated Speech Bubble](https://github.com/trolog/GodotanimatedSpeedbubble)
- State uses [GDScript Dictionary Merge](https://godotengine.org/qa/8024/update-dictionary-method)

### Tutorials

- ConversationPlayer NinePatchRect based on [How to make a Dialog Box in Godot](https://www.youtube.com/watch?v=YrjDsC4gRZ8)
- [Godot Animated Speech Bubble](https://github.com/trolog/GodotanimatedSpeedbubble)

## License
