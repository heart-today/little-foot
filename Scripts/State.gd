class_name State
	
# null if no action found
# {} if action is actually empty...might want to check that!
static func next_state(states : Dictionary, current : Dictionary, action : String, to: String = '*') -> Dictionary:
	var delta = null # {}
	if action in current:
		var next = current[action]
		if typeof(next) == TYPE_DICTIONARY:
			if to in next:
				next = next[to]
			elif '*' in next:
				# Look for default
				next = next['*']
		if next in states:
			delta = states[next]
			change_state(current, delta)
	return delta

# Just recursively merge dictionaries
# Found at https://godotengine.org/qa/8024/update-dictionary-method
static func change_state(target : Dictionary, patch : Dictionary) -> void:
	for key in patch:
		if target.has(key):
			var tv = target[key]
			if typeof(tv) == TYPE_DICTIONARY:
				change_state(tv, patch[key])
			else:
				target[key] = patch[key]
		else:
			target[key] = patch[key]

