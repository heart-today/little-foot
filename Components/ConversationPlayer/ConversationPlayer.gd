extends Node

# TODO
# Clean up this code for checking when to set various things visible.
# Didn't have time to think it through, so just iterated, and it got UGLY...heh

var player = null setget set_player

signal playing(yes_or_no)

onready var speakerLabel = $Control/BodyNinePatchRect/SpeakerNinePatchRect/Label
onready var bodyLabel = $Control/BodyNinePatchRect/MarginContainer/VBoxContainer/Label
onready var actionBar = $Control/BodyNinePatchRect/MarginContainer/VBoxContainer/ActionBar
onready var askButton = $Control/BodyNinePatchRect/MarginContainer/VBoxContainer/ActionBar/AskButton
onready var takeButton = $Control/BodyNinePatchRect/MarginContainer/VBoxContainer/ActionBar/TakeButton
onready var giveButton = $Control/BodyNinePatchRect/MarginContainer/VBoxContainer/ActionBar/GiveButton
onready var speakerRect = $Control/BodyNinePatchRect/SpeakerNinePatchRect

onready var timer = $Timer

func _ready():
	timer.connect("timeout", self, "next_conversation")
#	timer.start(.1)
	
var conversations = []
var messages = []

func next_conversation():
	if conversations.empty():
		timer.start(.1)
	else:
		var msg = messages.pop_front()
		speakerLabel.text = msg.speaker
		bodyLabel.text = msg.text
		timer.start(msg.pause)
		
func set_player(new_player : Node):
	if player:
		player.disconnect("meet", self, "on_Player_meet")
		player.disconnect("take", self, "on_Player_take")	

	player = new_player
	
	var result
	result = player.connect("meet", self, "on_Player_meet")
	assert(result == OK)
	result = player.connect("take", self, "on_Player_take")
	assert(result == OK)
	
var playing = 0
func play(conversation : Dictionary):
	playing += 1
	bodyLabel.visible = true
	speakerRect.visible = true
	$Control.visible = true
	for msg in conversation.exchange:
		var speaker = msg.speaker
		match speaker:
			"PLAYER": speaker = player.name
			"CHARACTER": speaker = conversation.character
				
		speakerLabel.text = speaker
		bodyLabel.text = msg.text
		yield(get_tree().create_timer(msg.pause), "timeout")

	playing -= 1
	if not playing:	
		speakerLabel.text = ""
		bodyLabel.text = ""
		
		bodyLabel.visible = false
		speakerRect.visible = false
		actionBar.visible = askButton.visible or takeButton.visible or giveButton.visible
		$Control.visible = actionBar.visible
	else:
		bodyLabel.visible = true
		speakerRect.visible = true
		$Control.visible = true
	
func show_character_icon(icon : Sprite):
	pass

func on_Player_meet(character : Node):
	askButton.visible = false if character == null else true
	takeButton.visible = false if character == null else true
	actionBar.visible = askButton.visible or takeButton.visible or giveButton.visible
	$Control.visible = actionBar.visible or playing
			
func on_Player_take(character : Node):
	askButton.visible = false if character == null else true
	giveButton.visible = false if character == null else true
	actionBar.visible = askButton.visible or takeButton.visible or giveButton.visible
	$Control.visible = actionBar.visible or playing
	
func _on_converse(converse : Dictionary):
	play(converse)

func _on_AskButton_pressed():
	assert(player)
	player.ask()

func _on_TakeButton_pressed():
	assert(player)
	player.take()

func _on_GiveButton_pressed():
	assert(player)
	player.give()
