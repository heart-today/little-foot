extends Node2D

onready var speechBubble = $SpeechBubble

# Called when the node enters the scene tree for the first time.
func _ready():
	speechBubble.set_text("What a wonderful [color=red]World[/color]!")
	speechBubble.set_text("Welcome to Little-Foot-Landia!")	

