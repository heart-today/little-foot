extends KinematicBody2D
class_name Character2D

signal converse(conversation)


# TODO
# Automatically load JSON states/conversations from res:// root by path
# -  No need to specify explicitly (though can override)

# Character is/contains:
# 
# StateMachine
# - next_state
# 
# Conversations
# 

onready var speechBubble = $SpeechBubble
func speak(msg):
	speechBubble.set_text(msg, 5)
	
const MEET = 'meet'
const ASK = 'ask'
const GIVE = 'give'
const TAKE = 'take'
const LEAVE = 'leave'

const MESSAGE = 'message'
const CONVERSATION = 'conversation'
const TRIGGER = 'trigger'

# For when everythings is a Character2D
const REJECT = 'reject'
const ACCEPT = 'accept'

var current_state = { can_take = true, can_give = false }
var states = {}
export (String, FILE, "*.json") var states_json

var conversations = {}
export (String, FILE, "*.json") var conversations_json

func _ready():
	# Load conversations first, since states use them
	load_conversations()
	load_states()
	
	timer.connect("timeout", self, "next_sound")
	next_sound()


func meet():
	next_state(MEET)

func ask():
	next_state(ASK)
	
func leave():
	next_state(LEAVE)
			
func can_take():
	next_state(TAKE)
	return current_state.can_take

func release():
	get_parent().remove_child(self)
	
	velocity = Vector2.ZERO
	$CollisionShape2D.disabled = true
	
	return self
	
func receive(character : Character2D):
	next_state(GIVE, character.name)
	return current_state.can_give
	
func next_state(action : String, to : String = '*'):
	current_state.erase(CONVERSATION)
	State.next_state(states, current_state, action, to)
	if CONVERSATION in current_state:
		var conversation_name = current_state[CONVERSATION]
		var exchange
		if conversation_name in conversations:
			exchange = conversations[conversation_name]
		else:
			exchange = [
				{
					speaker = name,
					text = conversation_name,
					pause = 5
				}
			]
		var conversation = {
			character = name,
			exchange = exchange
		}
		emit_signal("converse", conversation)
		
	if TRIGGER in current_state:
		var trigger = current_state[TRIGGER]
		if has_method(trigger):
			call(trigger, current_state)

# https://godotengine.org/qa/57130/how-to-import-and-read-text
func load_script_file(file, pause = 5):
	var f = File.new()
	f.open(file, File.READ)
	
	var name = null
	var index = 1
	while not f.eof_reached(): # iterate through all lines until the end of file is reached
		var line = f.get_line()
		
		var d = {}
		var x = []
		var t = line.trim_prefix("#")
		# Not the name
		if t == line:		
			assert(name)
			var words = line.split(':', false, 1)
			d[name].append({
				speaker = words[0],
				text = words[1],
				pause = pause
			})
		else:				
			name = t
			d[name] = []

		index += 1
	f.close()
	return
	
func load_json(file_name):
# Will contain the JSON parsing result if valid (typically a dictionary or array)
	var result

	var file = File.new()
	var file_error = file.open(file_name, File.READ)

	if file_error == OK:
		# The file was read successfully
		var json_result := JSON.parse(file.get_as_text())

		if json_result.error == OK:
			# File contains valid JSON
			result = json_result.result
		else:
			# File contains invalid JSON
			push_error(
					"Error while parsing JSON at {file_name}:{line}: {message}".format({
							file_name = file_name,
							line = json_result.error_line,
							message = json_result.error_string,
					})
			)
	else:
		# An error occurred related to the file reading (e.g. the file wasn't found)
		push_error("Could not open file: %s" % file_name)
		
	return result
	

func load_states():
	var result = load_json(states_json)
	if result:
		current_state = result.current
		states = result.states
		validate_states()

# TODO Validate trigger methods exist
# TODO Validate trigger methods take one Conversation?
func validate_states():
	for state_name in states:
		var state = states[state_name]
		if CONVERSATION in state:
			var conversation_name = state[CONVERSATION]
			if not conversation_name in conversations:
				push_warning("no conversation %s for character %s" % [conversation_name, name])
	
func load_conversations():
	var result = load_json(conversations_json)
	if result:
		conversations = result
		validate_conversations()

# TODO Validate members of each message
func validate_conversations():
	pass



export var randSoundTimeMin := 5
export var randSoundTimeMax := 10
export var playing := true setget set_playing

onready var timer = $Timer
onready var sound = $Sound

func set_playing(value):
	playing = value
	if playing and timer:
		timer.start()
	elif timer:
		timer.stop()
		


func next_sound():
	var r = randSoundTimeMax - randSoundTimeMin
	var time = randSoundTimeMin + randi() % r
	timer.start(time)
	sound.play()


export var FRICTION = 10

var velocity := Vector2.ZERO

onready var sprite = $Sprite

func _physics_process(delta):
	velocity = velocity.move_toward(Vector2.ZERO, FRICTION * delta)
		
	var collision = move_and_collide(velocity * delta)
	if collision:
		velocity = velocity.bounce(collision.normal)
		if collision.collider.has_method("hit"):
			collision.collider.hit()

