[[_TOC_]]

# Story Summary

[Oh so Rouch Sketch of LittleFootLandia](https://aggie.io/nxmhpwvmqg)

| West | Center | East |
| ---      |  ------  |----------|
| Dam   | FantasyForest   | Void   |
| River | Garden | Void  |
| Bedroom   | Void         | RockyOutcropping   |

1. LittleFoot starts in RockyOutcropping, traumatized by LostBody.
2. Meets Rock, asks if they have seen the LostBody or would like to be the LostBody?
3. Meets Oregano, asks if they have seen the LostBody or would like to be the LostBody?
4. Gives Oregano to GrassPatch who gives LittleFoot GrassSeeds for creating GrassTiles to cross the Void.
5. LittleFoot meets the Gardener, who needs Oregano to give to BigBarryThe Beaver.
6. Gardener thinks BigBarryTheBeaver may know where the LostBody is.
7. LittleFoot gives Oregano to Gardener and takes PottedOregano through FantasyForest.
8. LittleFoot sees Squirrels running back and forth in the FantasyForest.
9. LittleFoot meets ChiChi, who grabs LittleFoot, dropping PottedOregano, and takes LittleFoot back to GardenFantasyForestEdge.
10. LittleFoot cannot pass ChiChi.
11. LittleFoot goes back, gets GrassSeeds, gives to Gardener, who UnlocksGarden.
12. LittleFoot takes Nuts, and gives Nuts to Squirrel, then takes Squirrel.
13. LittleFoot gives Squirrel to ChiChi, who both run laughing through the FantasyForest.
14. LittleFoot gives BigBarryTheBeaver the PottedOregano.
15. BigBarryTheBeaver tells LittleFoot that the LostBody is at the end of the River.
16. BigBarryTheBeaver gives LittleFoot a RaftOfTwigs
17. LittleFoot gives RaftOfTwigs to the River, hops on and rides down the River...
18. And the DreamingWoman wakes up in the Bedroom, saying "What a bizarre dream!"
19. She picks up her prosthetic foot off the floor and hugs it to her chest in bed saying

    "Don't worry, Little Foot...I won't lose you, too."


# Characters

- LittleFoot (LF)
- LostBody (LB)
- Rock (R)
- Oregano (O)
- GrassPatch (GP)
- GrassSeeds (GS)
- GrassTile (GT)
- Gardener (G)
- PotForPlant (P4P)
- PottedOregano (PO)
- Nuts (N&S)
- Tomato (T)
- Pineapple (Pine)
- Squirrel(s) (Sq)
- ChiChiTheDog (Chi)
- BigBarryTheBeaver (B)
- RaftOfTwigs (R)
- Dreaming Woman (DW)
- Bats (B)

# Terrains

- Void - the emptiness whenever there isn’t other terrain, where the Bats dwell
- RockyOutcropping - gray ground, rock boundaries, perhaps slightly bigger than screen, but if move to edges, quickly see limits of walls, and emptiness beyond
- Garden - home of the Gardener, a couple of little tended patches of earth, a few plants and trees, green yet tame and soft and mild, home of N&S, T, and P
- Forest - beige path with trees on the sides, home of the Squirrel(s) and ChiChi the Dog
  - Tree
- Dam - home of Beaver and the Raft of Twigs
- River - water flowing away from Dam, path of Raft of Twigs
- Bedroom - home of Dreaming Woman (END GAME)

# Terrain Characters

- RockyOutcropping
  - Rock
  - Oregano
  - GrassPatch
  - GrassSeeds
  - GrassTile
- Void
  
  Nothing for Void as yet...
- Garden
  - Gardener (G)
  - PotForPlant (P4P)
  - PottedOregano (PO)
  - Nuts (N&S)
  - Tomato (T)
  - Pineapple (Pine)
- FantasyForest
  - Squirrel(s)
  - ChiChi (BearDog/DogBear)
  - Tree
- Dam
  - BigBarryTheBeaver
  - RaftOfTwigs
- River
- Bedroom
  - DreamingWoman
  - LittleProstheticFoot

# Terrain Interactions

- RockyOutcropping
  - MEET Rock
  - MEET Oregano
  - MEET GrassPatch
  - MEET(?) GrassSeeds MAKE GrassTile
- Void
  - LF(GS) MAKE GrassTile
- Garden
  - MEET Gardener
  - Gardener WANT Oregano for BigBarryTheBeaver
  - Gardener WANT GrassSeed for <GardenUnlocked>
  - LF MOVE, LF MEET Oregano TAKE, LF(Oregano)
  - LF(O) MEET Gardener GIVE LF(PottedOregano)
  - Gardener COMBINES Oregano + PotForPlant GIVES PottedOregano
  - LF(GS) MEET Gardener GIVE <GardenUnlocked>
  - LF<GU>(PO) MOVES North

<TO BE CONTINUED>

# Little Foot Actions


- MOVE
  - LittleFoot can move around terrains
  - When moving, LittleFoot MEETS Characters
- MEET 
  - When LittleFoot MEETS Characters, ASK & TAKE Buttons appear
  - LittleFoot can only MEET 1 Character at a time
  - (OPTIONAL) Intro dialog between LittleFoot and Character before ASK
    - Implies LF MEET * becomes LF<MET*>, *<MET>
- ASK 
  - Starts dialog with Characters (Button in DialogActionBox)
  - LittleFoot generally asks 2 questions:
    1. Have you seen my body?
    2. Would you like to be my body?
- TAKE
  - Take Character as body (Button in DialogActionBox)
  - Can only take Character if LF MEET Character
  - Character may not allow it
  - Character may SAY something in response to TAKE
  - LittleFoot then has Character on top of LittleFoot
  - If LittleFoot already has a Character TAKEN
    - LittleFoot GIVES current Character
    - TAKES new Character (if allowed)
  - When TAKE Character, GIVE button appears
  - LF MEET Character TAKE becomes LF(Character)
  - If no TAKE Character, LF()
- GIVE
  - Give Character away (Button in DialogActionBox)
  - IF LF(), GIVE Action is unavailable
  - If LF(*) MEET ?, GIVE * to ?
  - If no MEET Character, GIVE BodyCharacter to terrain 

# Visuals While Playing

- Shows terrain with Little Foot centered as LF meets other Characters
- Bottom part of screen is DialogAndActionBox for actions and text
- When LF MEETS Character(s), ActionRow shows up with ASK and TAKE
- When LF TALKS, DialogBox just shows text
- When LF ASKS Character
  - LF show on right, text in middle, then clear
  - Character show on left, text in middle, then clear
  - Repeat until done
  - (if time, we can change based on where LF is relative to Character)
- DialogBox allows 2 Characters to talk as well (see GrassPatch and Oregano below)


# Script

Scene:  Little Foot in the middle of Rocky Outcropping

Little Foot:  What the...?

*LF starts jittering*

LF: Where am I?

*LF more agitated*

LF: WHERE IS MY BODY?!?!??!

*LF hopping up and down*

LF: *pant* *pant*

LF: Okay...let's look around

LF: Maybe my body...just got lost...?

*LittleFoot wanders around*

*LittleFoot MEETS Rock*

LF: Oh, look...a rock.

LF MEET Rock ASK

LF: #1 Have you seen my body?

Rock: ...

LF MEET Rock ASK

LF: #2 Would you like to be my body?

Rock: ...

LF: Hm...rocks don't say much.

LF MEET Rock TAKE

*LittleFoot now has Rock above*

LF: Well...hmph.

*pause*

LF: Let's keep looking around

*LittleFoot MEETS Oregano*

LF: What a cute little plant!

O: I am a delicious Oregno!

LF MEET Oregano ASK

LF: #1?

O: Oh, no, I have not.

O: Plants don't get around much. *sigh*

LF MEET Oregano ASK

LF: #2?

O: Oh boy!  Yes please!  Let's go!

LF MEET O TAKE

O: We are on an adventure!

*LittleFoot(Oregano) wander around*

LF(O) GIVE

*O drops to the ground*

O:  That's it...?  *sigh*

LittleFoot at edge of RockyOutcropping, surrounded by Void

*LF MEETS GrassPatch*

LF: Ooh...this is so nice on me.

LF: Kind of tickles.

LF MEET GP ASK

LF: #1?

GP: No, I have not.  Perhaps the Gardener knows...

(OPTIONAL dialog LF MEET GP ASK about direction of Gardener)

LF: #2?

GP: No, thank you.

GP: I'm just sunning myself...

GP: Growing more seeds.

*LF goes and gets Oregano*

*LF(Oregano) MEETS GrassPatch*

LF(O) MEET GP ASK

LF(O): Would you like this little Oregano?

GP: Hm.  It would be nice...

GP: to have a companion

LF(O) MEET POG GIVE

GP: Hello, Oregano!

Oregano: Hello, Patch of Grass!

(OPTIONAL dual Character dialog, or simultaneous Character dialog)

Oregano & GP(Both icons showing): This is nice!

GP:  Thank you, LittleFoot.

GP:  I have grown some seeds...

GP:  Take them.

GP:  Perhaps you can spread them around.

LF TAKE GrassSeeds

LF(GS) MOVES

*LittleFoot has little swirl of GrassSeeds on top*

*When LittleFoot MOVES, GrassSeeds fall in front of LF, making GrassTile*

(OPTIONAL LF(GS) GIVE puts GS in front of LF, making GrassTile grow, but that might get really tedious...
LF(GS) GIVE, GS->GROW->GT(GS), LF() MOVE GT, LF MEET GS TAKE, LF(GS) GIVE...bleh.)

*Void fills with GrassTile until find Garden*

LF(GS) MEET Gardener ASK

G: Oh!  I have been wanting to grow more grass...

G:  My other grass seemed to wander away.

G:  Can I have those Grass Seeds?

LF(GS): #1?

G: No, I haven't seen your body, though

G: I think BigBarryTheBeaver may have mentioned 

G: A body at the end of the River.

G: I'm supposed to give that Beaver an Oregano plant

G: But all my Oregano seeds haven't sprouted yet.

G: Once they do, you can take the Oregano to him

G: And I'm sure he'll help you find your body.

LF(GS) MEET G GIVE

G: Oh, wonderful!  Thank you for the seeds.

G: I'll start growing grass again!

G: Help yourself to anything you find around the Garden!

* Little Foot now has access to GardenUnlocked - aka LF<GU>*

LF MEET Garden(*) TAKE

G: You greedy little thing...no * for you.

LF<GU> MEET Garden(*) TAKE

LF<GU>(*)

<TO BE CONTINUED>


